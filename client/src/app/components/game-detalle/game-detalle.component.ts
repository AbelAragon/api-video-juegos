import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-game-detalle',
  templateUrl: './game-detalle.component.html',
  styleUrls: ['./game-detalle.component.css']
})
export class GameDetalleComponent implements OnInit {

  identificador: any;
  contador = 1;

  lista:string[]=["Xbox","PS4","Wii", "Nintendo"];

  @HostBinding('class') clases = 'row';

  game: any = {
    id: 0,
    title: '',
    precio: 0,
    description: '',
    image: '',
    created_at: new Date()
  };

  constructor(private gameService: GamesService, private router: Router, private activatedRoute: ActivatedRoute) { }
  ngOnInit(): void {

    const params = this.activatedRoute.snapshot.params;
    if (params['id']) {
      this.gameService.getGame(params['id'])
        .subscribe(
          res => {
            console.log(res);
            this.game = res;
            document.getElementById('PagoTotal')?.setAttribute('value', this.game.precio);
          },
          err => console.log(err)
        )
    }
  }

  public sumaInput() {
    this.contador++

    var contador = this.contador;
    var precioDelJuego = this.game.precio;
    var calculoTotal = precioDelJuego * contador;

    var totalAPagar = calculoTotal.toString();
    var setContador = this.contador.toString();

    document.getElementById('cantidad')?.setAttribute('value', setContador);
    document.getElementById('PagoTotal')?.setAttribute('value', totalAPagar);
  }

  public restaInput(cantidad: string) {
    var cantidadProductos = parseInt(cantidad);
    if (cantidadProductos === 1) {
      this.contador = 1;
    } else {
      this.contador--;

      var contador = this.contador;
      var precioDelJuego = this.game.precio;
      var calculoTotal = precioDelJuego * contador;

      var totalAPagar = calculoTotal.toString();
      var setContador = this.contador.toString();

      document.getElementById('cantidad')?.setAttribute('value', setContador);
      document.getElementById('PagoTotal')?.setAttribute('value', totalAPagar);
    
    }
  }

  showModal(totalaPagar: string, tituloProducto: string) {
    console.log(this.game)
    Swal
      .fire({
        title: "Confirmación",
        text: 
        "Total a Pagar: $"+totalaPagar+" Producto "+tituloProducto,
        icon: 'info',
        showCancelButton: true,
        confirmButtonText: "Sí, confirmar",
        cancelButtonText: "Cancelar",
      })
      .then(resultado => {
        if (resultado.value) {
          Swal.fire('Hecho', 'Compra Realizada', 'success')
        } else {
          Swal.fire('Cancelada', 'No se ha surtido el producto', 'error')
        }
      });
  }
}