export interface Game{
   id?: number;
   title: string;
   precio: number;
   description: string;
   image?: string;
   created_at?: Date;
}